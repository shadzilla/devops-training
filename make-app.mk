USER = "$(shell id -u):$(shell id -g)"

app:
		docker-compose up

development-setup-env:
		ansible-playbook ansible/development.yml -i ansible/development -vv

app-build:
		docker-compose build

app-setup: development-setup-env app-build
		docker-compose run --user=$(USER) app bin/setup

app-bash:
		docker-compose run --user=$(USER) app bash