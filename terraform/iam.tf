resource "aws_iam_role" "itomsklog" {
  name = "itomsklog"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "applogs" {
    name        = "applogs"
    # description = "A test policy"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Action": [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:DescribeLogGroups",
              "logs:DescribeLogStreams",
              "logs:PutLogEvents",
              "logs:GetLogEvents",
              "logs:FilterLogEvents"
          ],
          "Effect": "Allow",
          "Resource": "*"
      }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "logs_to_webservers" {
    role       = "${aws_iam_role.itomsklog.name}"
    policy_arn = "${aws_iam_policy.applogs.arn}"
}

resource "aws_iam_instance_profile" "appitomsk" {
  name  = "apptomsk"
  role = "${aws_iam_role.itomsklog.name}"
}