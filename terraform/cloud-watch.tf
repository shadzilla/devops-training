resource "aws_cloudwatch_log_group" "itomsk" {
  name = "itomsk"
}

resource "aws_cloudwatch_log_stream" "app" {
  name           = "app"
  log_group_name = "${aws_cloudwatch_log_group.itomsk.name}"
}